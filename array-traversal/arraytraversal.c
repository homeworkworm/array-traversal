#include <stdio.h>
#include <stdlib.h>

const int indexStep = sizeof(int) / 4;

int arraysum(int *a)
{
	int sum = 0;
	int i;
	for (i = 0; i < 5; i++)
	{
		sum += a[i];
		printf("array value at index %d is %d and its memory location is 0x%p\n", i, a[i], &(a[i]));
	}
	return sum;
}

int arraysum_pointer(int *a)
{
	int sum = 0;
	int i;
	int *pointer;

	for (i = 0; i < 5; i++)
	{
		pointer = a + indexStep * i;
		sum += *pointer;
		*pointer = sum;
		printf("array value at index %d is %d and its memory location is 0x%p\n", i, *pointer, pointer);
	}
	return sum;
}

int arraysum_asm(int *a);

int parseInt(char *intText)
{
	int i = 0;
	char c;
	int sum = 0, digit;

	while ((c = intText[i]) != '\0')
	{
		digit = c - '0';
		sum *= 10;
		sum += digit;
		i++;
	}

	return sum;
}

int main(int argc, char *argv[])
{
	int i;
	int numbers[5];
	int switchNumber;
	int sum = 0;

	if (argc < 7)
	{
		printf("Insufficient number of arguments\n");
		return;
	}

	for (i = 0; i < 5; i++)
	{
		numbers[i] = parseInt(argv[i + 1]);
	}
	switchNumber = parseInt(argv[6]);

	switch (switchNumber)
	{
	case 1:
		sum = arraysum(numbers);
		break;
	case 2:
		sum = arraysum_pointer(numbers);
		break;
	case 3:
		sum = arraysum_asm(numbers);
		break;
	}
	printf("The sum of the array value is %d", sum);

	getchar();

	return EXIT_SUCCESS;
}